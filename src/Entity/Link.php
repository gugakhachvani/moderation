<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Link
 *
 * @ORM\Table(name="link", indexes={@ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="access_time", columns={"access_time"}), @ORM\Index(name="remote_user_id", columns={"remote_user_id"})})
 * @ORM\Entity
 */
class Link
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="access_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $accessTime;

    /**
     * @var int
     *
     * @ORM\Column(name="remote_user_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $remoteUserId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="message_text", type="string", length=128, nullable=false)
     */
    private $messageText;

    /**
     * @var int
     *
     * @ORM\Column(name="message_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $messageTime;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAccessTime(): ?string
    {
        return $this->accessTime;
    }

    public function setAccessTime(string $accessTime): self
    {
        $this->accessTime = $accessTime;

        return $this;
    }

    public function getRemoteUserId(): ?string
    {
        return $this->remoteUserId;
    }

    public function setRemoteUserId(string $remoteUserId): self
    {
        $this->remoteUserId = $remoteUserId;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getMessageText(): ?string
    {
        return $this->messageText;
    }

    public function setMessageText(string $messageText): self
    {
        $this->messageText = $messageText;

        return $this;
    }

    public function getMessageTime(): ?string
    {
        return $this->messageTime;
    }

    public function setMessageTime(string $messageTime): self
    {
        $this->messageTime = $messageTime;

        return $this;
    }


}
