<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item", indexes={@ORM\Index(name="facility_horse_walker", columns={"facility_horse_walker"}), @ORM\Index(name="livery_tailored_livery", columns={"livery_tailored_livery"}), @ORM\Index(name="published", columns={"published"}), @ORM\Index(name="livery_diy_livery", columns={"livery_diy_livery"}), @ORM\Index(name="facility_supplements_distribution", columns={"facility_supplements_distribution"}), @ORM\Index(name="service", columns={"service"}), @ORM\Index(name="facility_cold_shower", columns={"facility_cold_shower"}), @ORM\Index(name="location_country", columns={"location_country"}), @ORM\Index(name="facility_canter_track", columns={"facility_canter_track"}), @ORM\Index(name="facility_indoor_arena", columns={"facility_indoor_arena"}), @ORM\Index(name="publish_time", columns={"publish_time"}), @ORM\Index(name="livery_active_stable", columns={"livery_active_stable"}), @ORM\Index(name="facility_slow_feeders", columns={"facility_slow_feeders"}), @ORM\Index(name="livery_grass_livery", columns={"livery_grass_livery"}), @ORM\Index(name="facility_night_checks", columns={"facility_night_checks"}), @ORM\Index(name="location_region", columns={"location_region"}), @ORM\Index(name="facility_field_shelters", columns={"facility_field_shelters"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="facility_outdoor_arena", columns={"facility_outdoor_arena"}), @ORM\Index(name="livery_training_breacking_livery", columns={"livery_training_breacking_livery"}), @ORM\Index(name="facility_box_with_terrace", columns={"facility_box_with_terrace"}), @ORM\Index(name="livery_box_livery", columns={"livery_box_livery"}), @ORM\Index(name="facility_antifreeze_waterers", columns={"facility_antifreeze_waterers"}), @ORM\Index(name="location_zip_code", columns={"location_zip_code"}), @ORM\Index(name="facility_hot_shower", columns={"facility_hot_shower"}), @ORM\Index(name="insert_time", columns={"insert_time"})})
 * @ORM\Entity
 */
class Item
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="insert_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $insertTime;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $userId;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_grass_livery", type="boolean", nullable=false)
     */
    private $liveryGrassLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_box_livery", type="boolean", nullable=false)
     */
    private $liveryBoxLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_diy_livery", type="boolean", nullable=false)
     */
    private $liveryDiyLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_active_stable", type="boolean", nullable=false)
     */
    private $liveryActiveStable;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_training_breacking_livery", type="boolean", nullable=false)
     */
    private $liveryTrainingBreackingLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_tailored_livery", type="boolean", nullable=false)
     */
    private $liveryTailoredLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_indoor_arena", type="boolean", nullable=false)
     */
    private $facilityIndoorArena;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_outdoor_arena", type="boolean", nullable=false)
     */
    private $facilityOutdoorArena;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_horse_walker", type="boolean", nullable=false)
     */
    private $facilityHorseWalker;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_canter_track", type="boolean", nullable=false)
     */
    private $facilityCanterTrack;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_field_shelters", type="boolean", nullable=false)
     */
    private $facilityFieldShelters;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_hot_shower", type="boolean", nullable=false)
     */
    private $facilityHotShower;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_cold_shower", type="boolean", nullable=false)
     */
    private $facilityColdShower;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_night_checks", type="boolean", nullable=false)
     */
    private $facilityNightChecks;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_antifreeze_waterers", type="boolean", nullable=false)
     */
    private $facilityAntifreezeWaterers;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_supplements_distribution", type="boolean", nullable=false)
     */
    private $facilitySupplementsDistribution;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_slow_feeders", type="boolean", nullable=false)
     */
    private $facilitySlowFeeders;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_box_with_terrace", type="boolean", nullable=false)
     */
    private $facilityBoxWithTerrace;

    /**
     * @var int
     *
     * @ORM\Column(name="cover_image", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $coverImage;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2048, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="images", type="string", length=1024, nullable=false)
     */
    private $images;

    /**
     * @var string|null
     *
     * @ORM\Column(name="location_country", type="string", length=4, nullable=true)
     */
    private $locationCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="location_region", type="string", length=32, nullable=false)
     */
    private $locationRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="location_zip_code", type="string", length=32, nullable=false)
     */
    private $locationZipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=32, nullable=false)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean", nullable=false)
     */
    private $published;

    /**
     * @var string
     *
     * @ORM\Column(name="availability_availability", type="string", length=32, nullable=false)
     */
    private $availabilityAvailability;

    /**
     * @var string
     *
     * @ORM\Column(name="availability_phone", type="string", length=64, nullable=false)
     */
    private $availabilityPhone;

    /**
     * @var int
     *
     * @ORM\Column(name="availability_price_max", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $availabilityPriceMax;

    /**
     * @var int
     *
     * @ORM\Column(name="availability_price_min", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $availabilityPriceMin;

    /**
     * @var int
     *
     * @ORM\Column(name="publish_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $publishTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="availability_set", type="boolean", nullable=false)
     */
    private $availabilitySet;

    /**
     * @var bool
     *
     * @ORM\Column(name="explanation_set", type="boolean", nullable=false)
     */
    private $explanationSet;

    /**
     * @var bool
     *
     * @ORM\Column(name="facts_set", type="boolean", nullable=false)
     */
    private $factsSet;

    /**
     * @var int|null
     *
     * @ORM\Column(name="availability_from", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $availabilityFrom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="availability_to", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $availabilityTo;

    /**
     * @var int
     *
     * @ORM\Column(name="cover_image_small", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $coverImageSmall;

    /**
     * @var int
     *
     * @ORM\Column(name="cover_image_large", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $coverImageLarge;

    /**
     * @var int
     *
     * @ORM\Column(name="cover_image_original", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $coverImageOriginal;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getInsertTime(): ?string
    {
        return $this->insertTime;
    }

    public function setInsertTime(string $insertTime): self
    {
        $this->insertTime = $insertTime;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getLiveryGrassLivery(): ?bool
    {
        return $this->liveryGrassLivery;
    }

    public function setLiveryGrassLivery(bool $liveryGrassLivery): self
    {
        $this->liveryGrassLivery = $liveryGrassLivery;

        return $this;
    }

    public function getLiveryBoxLivery(): ?bool
    {
        return $this->liveryBoxLivery;
    }

    public function setLiveryBoxLivery(bool $liveryBoxLivery): self
    {
        $this->liveryBoxLivery = $liveryBoxLivery;

        return $this;
    }

    public function getLiveryDiyLivery(): ?bool
    {
        return $this->liveryDiyLivery;
    }

    public function setLiveryDiyLivery(bool $liveryDiyLivery): self
    {
        $this->liveryDiyLivery = $liveryDiyLivery;

        return $this;
    }

    public function getLiveryActiveStable(): ?bool
    {
        return $this->liveryActiveStable;
    }

    public function setLiveryActiveStable(bool $liveryActiveStable): self
    {
        $this->liveryActiveStable = $liveryActiveStable;

        return $this;
    }

    public function getLiveryTrainingBreackingLivery(): ?bool
    {
        return $this->liveryTrainingBreackingLivery;
    }

    public function setLiveryTrainingBreackingLivery(bool $liveryTrainingBreackingLivery): self
    {
        $this->liveryTrainingBreackingLivery = $liveryTrainingBreackingLivery;

        return $this;
    }

    public function getLiveryTailoredLivery(): ?bool
    {
        return $this->liveryTailoredLivery;
    }

    public function setLiveryTailoredLivery(bool $liveryTailoredLivery): self
    {
        $this->liveryTailoredLivery = $liveryTailoredLivery;

        return $this;
    }

    public function getFacilityIndoorArena(): ?bool
    {
        return $this->facilityIndoorArena;
    }

    public function setFacilityIndoorArena(bool $facilityIndoorArena): self
    {
        $this->facilityIndoorArena = $facilityIndoorArena;

        return $this;
    }

    public function getFacilityOutdoorArena(): ?bool
    {
        return $this->facilityOutdoorArena;
    }

    public function setFacilityOutdoorArena(bool $facilityOutdoorArena): self
    {
        $this->facilityOutdoorArena = $facilityOutdoorArena;

        return $this;
    }

    public function getFacilityHorseWalker(): ?bool
    {
        return $this->facilityHorseWalker;
    }

    public function setFacilityHorseWalker(bool $facilityHorseWalker): self
    {
        $this->facilityHorseWalker = $facilityHorseWalker;

        return $this;
    }

    public function getFacilityCanterTrack(): ?bool
    {
        return $this->facilityCanterTrack;
    }

    public function setFacilityCanterTrack(bool $facilityCanterTrack): self
    {
        $this->facilityCanterTrack = $facilityCanterTrack;

        return $this;
    }

    public function getFacilityFieldShelters(): ?bool
    {
        return $this->facilityFieldShelters;
    }

    public function setFacilityFieldShelters(bool $facilityFieldShelters): self
    {
        $this->facilityFieldShelters = $facilityFieldShelters;

        return $this;
    }

    public function getFacilityHotShower(): ?bool
    {
        return $this->facilityHotShower;
    }

    public function setFacilityHotShower(bool $facilityHotShower): self
    {
        $this->facilityHotShower = $facilityHotShower;

        return $this;
    }

    public function getFacilityColdShower(): ?bool
    {
        return $this->facilityColdShower;
    }

    public function setFacilityColdShower(bool $facilityColdShower): self
    {
        $this->facilityColdShower = $facilityColdShower;

        return $this;
    }

    public function getFacilityNightChecks(): ?bool
    {
        return $this->facilityNightChecks;
    }

    public function setFacilityNightChecks(bool $facilityNightChecks): self
    {
        $this->facilityNightChecks = $facilityNightChecks;

        return $this;
    }

    public function getFacilityAntifreezeWaterers(): ?bool
    {
        return $this->facilityAntifreezeWaterers;
    }

    public function setFacilityAntifreezeWaterers(bool $facilityAntifreezeWaterers): self
    {
        $this->facilityAntifreezeWaterers = $facilityAntifreezeWaterers;

        return $this;
    }

    public function getFacilitySupplementsDistribution(): ?bool
    {
        return $this->facilitySupplementsDistribution;
    }

    public function setFacilitySupplementsDistribution(bool $facilitySupplementsDistribution): self
    {
        $this->facilitySupplementsDistribution = $facilitySupplementsDistribution;

        return $this;
    }

    public function getFacilitySlowFeeders(): ?bool
    {
        return $this->facilitySlowFeeders;
    }

    public function setFacilitySlowFeeders(bool $facilitySlowFeeders): self
    {
        $this->facilitySlowFeeders = $facilitySlowFeeders;

        return $this;
    }

    public function getFacilityBoxWithTerrace(): ?bool
    {
        return $this->facilityBoxWithTerrace;
    }

    public function setFacilityBoxWithTerrace(bool $facilityBoxWithTerrace): self
    {
        $this->facilityBoxWithTerrace = $facilityBoxWithTerrace;

        return $this;
    }

    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    public function setCoverImage(string $coverImage): self
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImages(): ?string
    {
        return $this->images;
    }

    public function setImages(string $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getLocationCountry(): ?string
    {
        return $this->locationCountry;
    }

    public function setLocationCountry(?string $locationCountry): self
    {
        $this->locationCountry = $locationCountry;

        return $this;
    }

    public function getLocationRegion(): ?string
    {
        return $this->locationRegion;
    }

    public function setLocationRegion(string $locationRegion): self
    {
        $this->locationRegion = $locationRegion;

        return $this;
    }

    public function getLocationZipCode(): ?string
    {
        return $this->locationZipCode;
    }

    public function setLocationZipCode(string $locationZipCode): self
    {
        $this->locationZipCode = $locationZipCode;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getAvailabilityAvailability(): ?string
    {
        return $this->availabilityAvailability;
    }

    public function setAvailabilityAvailability(string $availabilityAvailability): self
    {
        $this->availabilityAvailability = $availabilityAvailability;

        return $this;
    }

    public function getAvailabilityPhone(): ?string
    {
        return $this->availabilityPhone;
    }

    public function setAvailabilityPhone(string $availabilityPhone): self
    {
        $this->availabilityPhone = $availabilityPhone;

        return $this;
    }

    public function getAvailabilityPriceMax(): ?string
    {
        return $this->availabilityPriceMax;
    }

    public function setAvailabilityPriceMax(string $availabilityPriceMax): self
    {
        $this->availabilityPriceMax = $availabilityPriceMax;

        return $this;
    }

    public function getAvailabilityPriceMin(): ?string
    {
        return $this->availabilityPriceMin;
    }

    public function setAvailabilityPriceMin(string $availabilityPriceMin): self
    {
        $this->availabilityPriceMin = $availabilityPriceMin;

        return $this;
    }

    public function getPublishTime(): ?string
    {
        return $this->publishTime;
    }

    public function setPublishTime(string $publishTime): self
    {
        $this->publishTime = $publishTime;

        return $this;
    }

    public function getAvailabilitySet(): ?bool
    {
        return $this->availabilitySet;
    }

    public function setAvailabilitySet(bool $availabilitySet): self
    {
        $this->availabilitySet = $availabilitySet;

        return $this;
    }

    public function getExplanationSet(): ?bool
    {
        return $this->explanationSet;
    }

    public function setExplanationSet(bool $explanationSet): self
    {
        $this->explanationSet = $explanationSet;

        return $this;
    }

    public function getFactsSet(): ?bool
    {
        return $this->factsSet;
    }

    public function setFactsSet(bool $factsSet): self
    {
        $this->factsSet = $factsSet;

        return $this;
    }

    public function getAvailabilityFrom(): ?string
    {
        return $this->availabilityFrom;
    }

    public function setAvailabilityFrom(?string $availabilityFrom): self
    {
        $this->availabilityFrom = $availabilityFrom;

        return $this;
    }

    public function getAvailabilityTo(): ?string
    {
        return $this->availabilityTo;
    }

    public function setAvailabilityTo(?string $availabilityTo): self
    {
        $this->availabilityTo = $availabilityTo;

        return $this;
    }

    public function getCoverImageSmall(): ?string
    {
        return $this->coverImageSmall;
    }

    public function setCoverImageSmall(string $coverImageSmall): self
    {
        $this->coverImageSmall = $coverImageSmall;

        return $this;
    }

    public function getCoverImageLarge(): ?string
    {
        return $this->coverImageLarge;
    }

    public function setCoverImageLarge(string $coverImageLarge): self
    {
        $this->coverImageLarge = $coverImageLarge;

        return $this;
    }

    public function getCoverImageOriginal(): ?string
    {
        return $this->coverImageOriginal;
    }

    public function setCoverImageOriginal(string $coverImageOriginal): self
    {
        $this->coverImageOriginal = $coverImageOriginal;

        return $this;
    }


}
