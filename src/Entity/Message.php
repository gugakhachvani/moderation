<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="remote_user_id", columns={"remote_user_id"}), @ORM\Index(name="type", columns={"type"}), @ORM\Index(name="insert_time", columns={"insert_time"}), @ORM\Index(name="unread", columns={"unread"}), @ORM\Index(name="read", columns={"read"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="string", length=2048, nullable=false)
     */
    private $data;

    /**
     * @var int
     *
     * @ORM\Column(name="insert_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $insertTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="read", type="boolean", nullable=false)
     */
    private $read;

    /**
     * @var int
     *
     * @ORM\Column(name="read_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $readTime;

    /**
     * @var int
     *
     * @ORM\Column(name="remote_user_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $remoteUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32, nullable=false)
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="unread", type="boolean", nullable=false)
     */
    private $unread;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $userId;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getInsertTime(): ?string
    {
        return $this->insertTime;
    }

    public function setInsertTime(string $insertTime): self
    {
        $this->insertTime = $insertTime;

        return $this;
    }

    public function getRead(): ?bool
    {
        return $this->read;
    }

    public function setRead(bool $read): self
    {
        $this->read = $read;

        return $this;
    }

    public function getReadTime(): ?string
    {
        return $this->readTime;
    }

    public function setReadTime(string $readTime): self
    {
        $this->readTime = $readTime;

        return $this;
    }

    public function getRemoteUserId(): ?string
    {
        return $this->remoteUserId;
    }

    public function setRemoteUserId(string $remoteUserId): self
    {
        $this->remoteUserId = $remoteUserId;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUnread(): ?bool
    {
        return $this->unread;
    }

    public function setUnread(bool $unread): self
    {
        $this->unread = $unread;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }


}
