<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Search
 *
 * @ORM\Table(name="search", indexes={@ORM\Index(name="facility_cold_shower", columns={"facility_cold_shower"}), @ORM\Index(name="facility_canter_track", columns={"facility_canter_track"}), @ORM\Index(name="facility_indoor_arena", columns={"facility_indoor_arena"}), @ORM\Index(name="livery_active_stable", columns={"livery_active_stable"}), @ORM\Index(name="facility_slow_feeders", columns={"facility_slow_feeders"}), @ORM\Index(name="livery_grass_livery", columns={"livery_grass_livery"}), @ORM\Index(name="facility_night_checks", columns={"facility_night_checks"}), @ORM\Index(name="facility_field_shelters", columns={"facility_field_shelters"}), @ORM\Index(name="facility_outdoor_arena", columns={"facility_outdoor_arena"}), @ORM\Index(name="livery_training_breacking_livery", columns={"livery_training_breacking_livery"}), @ORM\Index(name="facility_box_with_terrace", columns={"facility_box_with_terrace"}), @ORM\Index(name="livery_box_livery", columns={"livery_box_livery"}), @ORM\Index(name="facility_antifreeze_waterers", columns={"facility_antifreeze_waterers"}), @ORM\Index(name="insert_time", columns={"insert_time"}), @ORM\Index(name="facility_hot_shower", columns={"facility_hot_shower"}), @ORM\Index(name="facility_horse_walker", columns={"facility_horse_walker"}), @ORM\Index(name="livery_tailored_livery", columns={"livery_tailored_livery"}), @ORM\Index(name="livery_diy_livery", columns={"livery_diy_livery"}), @ORM\Index(name="facility_supplements_distribution", columns={"facility_supplements_distribution"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class Search
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="insert_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $insertTime;

    /**
     * @var string
     *
     * @ORM\Column(name="availability", type="string", length=32, nullable=false)
     */
    private $availability;

    /**
     * @var string
     *
     * @ORM\Column(name="direction", type="string", length=32, nullable=false)
     */
    private $direction;

    /**
     * @var int|null
     *
     * @ORM\Column(name="from", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $from;

    /**
     * @var int|null
     *
     * @ORM\Column(name="to", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $to;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $userId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="string", length=4, nullable=true)
     */
    private $country;

    /**
     * @var int
     *
     * @ORM\Column(name="distance", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $distance;

    /**
     * @var int
     *
     * @ORM\Column(name="price_max", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $priceMax;

    /**
     * @var int
     *
     * @ORM\Column(name="price_min", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $priceMin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="region", type="string", length=32, nullable=true)
     */
    private $region;

    /**
     * @var string|null
     *
     * @ORM\Column(name="service", type="string", length=32, nullable=true)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=32, nullable=false)
     */
    private $zipCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_grass_livery", type="boolean", nullable=false)
     */
    private $liveryGrassLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_box_livery", type="boolean", nullable=false)
     */
    private $liveryBoxLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_diy_livery", type="boolean", nullable=false)
     */
    private $liveryDiyLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_active_stable", type="boolean", nullable=false)
     */
    private $liveryActiveStable;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_training_breacking_livery", type="boolean", nullable=false)
     */
    private $liveryTrainingBreackingLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="livery_tailored_livery", type="boolean", nullable=false)
     */
    private $liveryTailoredLivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_indoor_arena", type="boolean", nullable=false)
     */
    private $facilityIndoorArena;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_outdoor_arena", type="boolean", nullable=false)
     */
    private $facilityOutdoorArena;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_horse_walker", type="boolean", nullable=false)
     */
    private $facilityHorseWalker;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_canter_track", type="boolean", nullable=false)
     */
    private $facilityCanterTrack;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_field_shelters", type="boolean", nullable=false)
     */
    private $facilityFieldShelters;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_hot_shower", type="boolean", nullable=false)
     */
    private $facilityHotShower;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_cold_shower", type="boolean", nullable=false)
     */
    private $facilityColdShower;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_night_checks", type="boolean", nullable=false)
     */
    private $facilityNightChecks;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_antifreeze_waterers", type="boolean", nullable=false)
     */
    private $facilityAntifreezeWaterers;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_supplements_distribution", type="boolean", nullable=false)
     */
    private $facilitySupplementsDistribution;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_slow_feeders", type="boolean", nullable=false)
     */
    private $facilitySlowFeeders;

    /**
     * @var bool
     *
     * @ORM\Column(name="facility_box_with_terrace", type="boolean", nullable=false)
     */
    private $facilityBoxWithTerrace;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getInsertTime(): ?string
    {
        return $this->insertTime;
    }

    public function setInsertTime(string $insertTime): self
    {
        $this->insertTime = $insertTime;

        return $this;
    }

    public function getAvailability(): ?string
    {
        return $this->availability;
    }

    public function setAvailability(string $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getFrom(): ?string
    {
        return $this->from;
    }

    public function setFrom(?string $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function getTo(): ?string
    {
        return $this->to;
    }

    public function setTo(?string $to): self
    {
        $this->to = $to;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDistance(): ?string
    {
        return $this->distance;
    }

    public function setDistance(string $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getPriceMax(): ?string
    {
        return $this->priceMax;
    }

    public function setPriceMax(string $priceMax): self
    {
        $this->priceMax = $priceMax;

        return $this;
    }

    public function getPriceMin(): ?string
    {
        return $this->priceMin;
    }

    public function setPriceMin(string $priceMin): self
    {
        $this->priceMin = $priceMin;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(?string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getLiveryGrassLivery(): ?bool
    {
        return $this->liveryGrassLivery;
    }

    public function setLiveryGrassLivery(bool $liveryGrassLivery): self
    {
        $this->liveryGrassLivery = $liveryGrassLivery;

        return $this;
    }

    public function getLiveryBoxLivery(): ?bool
    {
        return $this->liveryBoxLivery;
    }

    public function setLiveryBoxLivery(bool $liveryBoxLivery): self
    {
        $this->liveryBoxLivery = $liveryBoxLivery;

        return $this;
    }

    public function getLiveryDiyLivery(): ?bool
    {
        return $this->liveryDiyLivery;
    }

    public function setLiveryDiyLivery(bool $liveryDiyLivery): self
    {
        $this->liveryDiyLivery = $liveryDiyLivery;

        return $this;
    }

    public function getLiveryActiveStable(): ?bool
    {
        return $this->liveryActiveStable;
    }

    public function setLiveryActiveStable(bool $liveryActiveStable): self
    {
        $this->liveryActiveStable = $liveryActiveStable;

        return $this;
    }

    public function getLiveryTrainingBreackingLivery(): ?bool
    {
        return $this->liveryTrainingBreackingLivery;
    }

    public function setLiveryTrainingBreackingLivery(bool $liveryTrainingBreackingLivery): self
    {
        $this->liveryTrainingBreackingLivery = $liveryTrainingBreackingLivery;

        return $this;
    }

    public function getLiveryTailoredLivery(): ?bool
    {
        return $this->liveryTailoredLivery;
    }

    public function setLiveryTailoredLivery(bool $liveryTailoredLivery): self
    {
        $this->liveryTailoredLivery = $liveryTailoredLivery;

        return $this;
    }

    public function getFacilityIndoorArena(): ?bool
    {
        return $this->facilityIndoorArena;
    }

    public function setFacilityIndoorArena(bool $facilityIndoorArena): self
    {
        $this->facilityIndoorArena = $facilityIndoorArena;

        return $this;
    }

    public function getFacilityOutdoorArena(): ?bool
    {
        return $this->facilityOutdoorArena;
    }

    public function setFacilityOutdoorArena(bool $facilityOutdoorArena): self
    {
        $this->facilityOutdoorArena = $facilityOutdoorArena;

        return $this;
    }

    public function getFacilityHorseWalker(): ?bool
    {
        return $this->facilityHorseWalker;
    }

    public function setFacilityHorseWalker(bool $facilityHorseWalker): self
    {
        $this->facilityHorseWalker = $facilityHorseWalker;

        return $this;
    }

    public function getFacilityCanterTrack(): ?bool
    {
        return $this->facilityCanterTrack;
    }

    public function setFacilityCanterTrack(bool $facilityCanterTrack): self
    {
        $this->facilityCanterTrack = $facilityCanterTrack;

        return $this;
    }

    public function getFacilityFieldShelters(): ?bool
    {
        return $this->facilityFieldShelters;
    }

    public function setFacilityFieldShelters(bool $facilityFieldShelters): self
    {
        $this->facilityFieldShelters = $facilityFieldShelters;

        return $this;
    }

    public function getFacilityHotShower(): ?bool
    {
        return $this->facilityHotShower;
    }

    public function setFacilityHotShower(bool $facilityHotShower): self
    {
        $this->facilityHotShower = $facilityHotShower;

        return $this;
    }

    public function getFacilityColdShower(): ?bool
    {
        return $this->facilityColdShower;
    }

    public function setFacilityColdShower(bool $facilityColdShower): self
    {
        $this->facilityColdShower = $facilityColdShower;

        return $this;
    }

    public function getFacilityNightChecks(): ?bool
    {
        return $this->facilityNightChecks;
    }

    public function setFacilityNightChecks(bool $facilityNightChecks): self
    {
        $this->facilityNightChecks = $facilityNightChecks;

        return $this;
    }

    public function getFacilityAntifreezeWaterers(): ?bool
    {
        return $this->facilityAntifreezeWaterers;
    }

    public function setFacilityAntifreezeWaterers(bool $facilityAntifreezeWaterers): self
    {
        $this->facilityAntifreezeWaterers = $facilityAntifreezeWaterers;

        return $this;
    }

    public function getFacilitySupplementsDistribution(): ?bool
    {
        return $this->facilitySupplementsDistribution;
    }

    public function setFacilitySupplementsDistribution(bool $facilitySupplementsDistribution): self
    {
        $this->facilitySupplementsDistribution = $facilitySupplementsDistribution;

        return $this;
    }

    public function getFacilitySlowFeeders(): ?bool
    {
        return $this->facilitySlowFeeders;
    }

    public function setFacilitySlowFeeders(bool $facilitySlowFeeders): self
    {
        $this->facilitySlowFeeders = $facilitySlowFeeders;

        return $this;
    }

    public function getFacilityBoxWithTerrace(): ?bool
    {
        return $this->facilityBoxWithTerrace;
    }

    public function setFacilityBoxWithTerrace(bool $facilityBoxWithTerrace): self
    {
        $this->facilityBoxWithTerrace = $facilityBoxWithTerrace;

        return $this;
    }


}
