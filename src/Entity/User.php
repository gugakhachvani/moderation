<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", indexes={@ORM\Index(name="email", columns={"email"})})
 * @ORM\Entity
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="insert_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $insertTime;

    /**
     * @var binary
     *
     * @ORM\Column(name="password_hash", type="binary", nullable=false)
     */
    private $passwordHash;

    /**
     * @var int
     *
     * @ORM\Column(name="access_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $accessTime;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_company_name", type="string", length=128, nullable=false)
     */
    private $profileCompanyName;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_legality", type="string", length=32, nullable=false)
     */
    private $profileLegality;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_name", type="string", length=64, nullable=false)
     */
    private $profileName;

    /**
     * @var bool
     *
     * @ORM\Column(name="profile_set", type="boolean", nullable=false)
     */
    private $profileSet;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_surname", type="string", length=64, nullable=false)
     */
    private $profileSurname;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_company_image", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileCompanyImage;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_image", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileImage;

    /**
     * @var bool
     *
     * @ORM\Column(name="notify_message", type="boolean", nullable=false)
     */
    private $notifyMessage;

    /**
     * @var bool
     *
     * @ORM\Column(name="notify_match", type="boolean", nullable=false)
     */
    private $notifyMatch;

    /**
     * @var bool
     *
     * @ORM\Column(name="notify_promotion", type="boolean", nullable=false)
     */
    private $notifyPromotion;

    /**
     * @var bool
     *
     * @ORM\Column(name="notify_transaction", type="boolean", nullable=false)
     */
    private $notifyTransaction;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=2, nullable=false)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=64, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_biography", type="string", length=2048, nullable=false)
     */
    private $profileBiography;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_location", type="string", length=128, nullable=false)
     */
    private $profileLocation;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_begin_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $subscriptionBeginTime;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_end_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $subscriptionEndTime;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_length", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $subscriptionLength;

    /**
     * @var string
     *
     * @ORM\Column(name="subscription_plan", type="string", length=32, nullable=false)
     */
    private $subscriptionPlan;

    /**
     * @var bool
     *
     * @ORM\Column(name="subscription_set", type="boolean", nullable=false)
     */
    private $subscriptionSet;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_company_image_small", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileCompanyImageSmall;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_image_small", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileImageSmall;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_company_image_large", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileCompanyImageLarge;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_image_large", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileImageLarge;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_company_image_original", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileCompanyImageOriginal;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_image_original", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $profileImageOriginal;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getInsertTime(): ?string
    {
        return $this->insertTime;
    }

    public function setInsertTime(string $insertTime): self
    {
        $this->insertTime = $insertTime;

        return $this;
    }

    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    public function setPasswordHash($passwordHash): self
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    public function getAccessTime(): ?string
    {
        return $this->accessTime;
    }

    public function setAccessTime(string $accessTime): self
    {
        $this->accessTime = $accessTime;

        return $this;
    }

    public function getProfileCompanyName(): ?string
    {
        return $this->profileCompanyName;
    }

    public function setProfileCompanyName(string $profileCompanyName): self
    {
        $this->profileCompanyName = $profileCompanyName;

        return $this;
    }

    public function getProfileLegality(): ?string
    {
        return $this->profileLegality;
    }

    public function setProfileLegality(string $profileLegality): self
    {
        $this->profileLegality = $profileLegality;

        return $this;
    }

    public function getProfileName(): ?string
    {
        return $this->profileName;
    }

    public function setProfileName(string $profileName): self
    {
        $this->profileName = $profileName;

        return $this;
    }

    public function getProfileSet(): ?bool
    {
        return $this->profileSet;
    }

    public function setProfileSet(bool $profileSet): self
    {
        $this->profileSet = $profileSet;

        return $this;
    }

    public function getProfileSurname(): ?string
    {
        return $this->profileSurname;
    }

    public function setProfileSurname(string $profileSurname): self
    {
        $this->profileSurname = $profileSurname;

        return $this;
    }

    public function getProfileCompanyImage(): ?string
    {
        return $this->profileCompanyImage;
    }

    public function setProfileCompanyImage(string $profileCompanyImage): self
    {
        $this->profileCompanyImage = $profileCompanyImage;

        return $this;
    }

    public function getProfileImage(): ?string
    {
        return $this->profileImage;
    }

    public function setProfileImage(string $profileImage): self
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    public function getNotifyMessage(): ?bool
    {
        return $this->notifyMessage;
    }

    public function setNotifyMessage(bool $notifyMessage): self
    {
        $this->notifyMessage = $notifyMessage;

        return $this;
    }

    public function getNotifyMatch(): ?bool
    {
        return $this->notifyMatch;
    }

    public function setNotifyMatch(bool $notifyMatch): self
    {
        $this->notifyMatch = $notifyMatch;

        return $this;
    }

    public function getNotifyPromotion(): ?bool
    {
        return $this->notifyPromotion;
    }

    public function setNotifyPromotion(bool $notifyPromotion): self
    {
        $this->notifyPromotion = $notifyPromotion;

        return $this;
    }

    public function getNotifyTransaction(): ?bool
    {
        return $this->notifyTransaction;
    }

    public function setNotifyTransaction(bool $notifyTransaction): self
    {
        $this->notifyTransaction = $notifyTransaction;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getProfileBiography(): ?string
    {
        return $this->profileBiography;
    }

    public function setProfileBiography(string $profileBiography): self
    {
        $this->profileBiography = $profileBiography;

        return $this;
    }

    public function getProfileLocation(): ?string
    {
        return $this->profileLocation;
    }

    public function setProfileLocation(string $profileLocation): self
    {
        $this->profileLocation = $profileLocation;

        return $this;
    }

    public function getSubscriptionBeginTime(): ?string
    {
        return $this->subscriptionBeginTime;
    }

    public function setSubscriptionBeginTime(string $subscriptionBeginTime): self
    {
        $this->subscriptionBeginTime = $subscriptionBeginTime;

        return $this;
    }

    public function getSubscriptionEndTime(): ?string
    {
        return $this->subscriptionEndTime;
    }

    public function setSubscriptionEndTime(string $subscriptionEndTime): self
    {
        $this->subscriptionEndTime = $subscriptionEndTime;

        return $this;
    }

    public function getSubscriptionLength(): ?string
    {
        return $this->subscriptionLength;
    }

    public function setSubscriptionLength(string $subscriptionLength): self
    {
        $this->subscriptionLength = $subscriptionLength;

        return $this;
    }

    public function getSubscriptionPlan(): ?string
    {
        return $this->subscriptionPlan;
    }

    public function setSubscriptionPlan(string $subscriptionPlan): self
    {
        $this->subscriptionPlan = $subscriptionPlan;

        return $this;
    }

    public function getSubscriptionSet(): ?bool
    {
        return $this->subscriptionSet;
    }

    public function setSubscriptionSet(bool $subscriptionSet): self
    {
        $this->subscriptionSet = $subscriptionSet;

        return $this;
    }

    public function getProfileCompanyImageSmall(): ?string
    {
        return $this->profileCompanyImageSmall;
    }

    public function setProfileCompanyImageSmall(string $profileCompanyImageSmall): self
    {
        $this->profileCompanyImageSmall = $profileCompanyImageSmall;

        return $this;
    }

    public function getProfileImageSmall(): ?string
    {
        return $this->profileImageSmall;
    }

    public function setProfileImageSmall(string $profileImageSmall): self
    {
        $this->profileImageSmall = $profileImageSmall;

        return $this;
    }

    public function getProfileCompanyImageLarge(): ?string
    {
        return $this->profileCompanyImageLarge;
    }

    public function setProfileCompanyImageLarge(string $profileCompanyImageLarge): self
    {
        $this->profileCompanyImageLarge = $profileCompanyImageLarge;

        return $this;
    }

    public function getProfileImageLarge(): ?string
    {
        return $this->profileImageLarge;
    }

    public function setProfileImageLarge(string $profileImageLarge): self
    {
        $this->profileImageLarge = $profileImageLarge;

        return $this;
    }

    public function getProfileCompanyImageOriginal(): ?string
    {
        return $this->profileCompanyImageOriginal;
    }

    public function setProfileCompanyImageOriginal(string $profileCompanyImageOriginal): self
    {
        $this->profileCompanyImageOriginal = $profileCompanyImageOriginal;

        return $this;
    }

    public function getProfileImageOriginal(): ?string
    {
        return $this->profileImageOriginal;
    }

    public function setProfileImageOriginal(string $profileImageOriginal): self
    {
        $this->profileImageOriginal = $profileImageOriginal;

        return $this;
    }


}
