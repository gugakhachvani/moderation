<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailEdit
 *
 * @ORM\Table(name="email_edit", indexes={@ORM\Index(name="insert_time", columns={"insert_time"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class EmailEdit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="insert_time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $insertTime;

    /**
     * @var binary
     *
     * @ORM\Column(name="private_key_hash", type="binary", nullable=false)
     */
    private $privateKeyHash;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $userId;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getInsertTime(): ?string
    {
        return $this->insertTime;
    }

    public function setInsertTime(string $insertTime): self
    {
        $this->insertTime = $insertTime;

        return $this;
    }

    public function getPrivateKeyHash()
    {
        return $this->privateKeyHash;
    }

    public function setPrivateKeyHash($privateKeyHash): self
    {
        $this->privateKeyHash = $privateKeyHash;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }


}
