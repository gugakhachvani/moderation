<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemTime
 *
 * @ORM\Table(name="item_time", indexes={@ORM\Index(name="item_id", columns={"item_id"}), @ORM\Index(name="time", columns={"time"})})
 * @ORM\Entity
 */
class ItemTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="item_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $itemId;

    /**
     * @var int
     *
     * @ORM\Column(name="time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $time;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    public function setItemId(string $itemId): self
    {
        $this->itemId = $itemId;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }


}
