<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SearchTime
 *
 * @ORM\Table(name="search_time", indexes={@ORM\Index(name="search_id", columns={"search_id"}), @ORM\Index(name="time", columns={"time"})})
 * @ORM\Entity
 */
class SearchTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="search_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $searchId;

    /**
     * @var int
     *
     * @ORM\Column(name="time", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $time;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSearchId(): ?string
    {
        return $this->searchId;
    }

    public function setSearchId(string $searchId): self
    {
        $this->searchId = $searchId;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }


}
