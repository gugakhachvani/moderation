<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'profile')]
    public function index(): Response
    {   
        $userData = array();
        $userinfo = array();
        $product = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        foreach($product as $produnio){ 
            $userInfo['id'] = $produnio->getId();
            $userInfo['name'] = $produnio->getProfileCompanyName();            
            $userInfo['regTime'] = gmdate("Y-m-d\TH:i:s\Z", $produnio->getInsertTime());
            if($produnio->getProfileImageLarge() == 0) {continue;}            
            $folder = $produnio->getProfileImageLarge() % 10;            
            $id = $produnio->getProfileImageLarge();
            $imgrd = chunk_split(base64_encode(file_get_contents('/home/user/sites/horsecare24.com.dev.neqson.net/www/data/file/'.$folder.'/'.$id)));  
            $userInfo['img'] = $imgrd; 
            array_push($userData, $userInfo);
        }
        return $this->json([
            'message' => 'success',
            'userData' => $userData            
        ]);
    }   
}
