<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Item;
use Doctrine\ORM\EntityManagerInterface;

class AdsController extends AbstractController
{
    #[Route('/ads', name: 'ads')]
    public function index(): Response
    {   
        
        $userData = array();
        $userinfo = array();
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT I.id as id, I.cover_image_large as img, I.title as title, I.insert_time as time from item I left join user U on I.user_id = U.id ");
        $statement->execute();
        $results = $statement->fetchAll();
        foreach($results as $produnio){ 
            $userInfo['id'] = $produnio['id'];
            $userInfo['name'] = $produnio['title'];            
            $userInfo['regTime'] = gmdate("Y-m-d\TH:i:s\Z", $produnio['time']);
            if($produnio['img'] == 0) {continue;}            
            $folder = $produnio['img'] % 10;            
            $id = $produnio['img'];
            $imgrd = chunk_split(base64_encode(file_get_contents('/home/user/sites/horsecare24.com.dev.neqson.net/www/data/file/'.$folder.'/'.$id)));  
            $userInfo['img'] = $imgrd; 
            array_push($userData, $userInfo);
        }
        return $this->json([
            'message' => 'success',
            'userData' => $userData            
        ]);
    }   
}
