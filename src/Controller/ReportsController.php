<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Report;
use App\Entity\Item;
use Doctrine\ORM\EntityManagerInterface;

class ReportsController extends AbstractController
{
    #[Route('/ads', name: 'ads')]
    public function index(): Response
    {   
        
        $userData = array();
        $userinfo = array();
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT R.id as id, I.cover_image_small as img, R.text as text, R.insert_time as time from report R left join item I on R.item_id = I.id ");
        $statement->execute();
        $results = $statement->fetchAll();
        foreach($results as $produnio){ 

            $userInfo['id'] = $produnio['id'];
            $userInfo['text'] = $produnio['text'];            
            $userInfo['regTime'] = gmdate("Y-m-d\TH:i:s\Z", $produnio['time']);
            $folder = $produnio['img'] % 10;
            $id = $produnio['img'];
            if(($data = @file_get_contents('/home/user/sites/horsecare24.com.dev.neqson.net/www/data/file/'.$folder.'/'.$id)) === false){
                $imgrd = '';
            }else{
                $imgrd = chunk_split(base64_encode(file_get_contents('/home/user/sites/horsecare24.com.dev.neqson.net/www/data/file/'.$folder.'/'.$id)));
            }
              
            $userInfo['img'] = $imgrd; 
            array_push($userData, $userInfo);
        }
        return $this->json([
            'message' => 'success',
            'userData' => $userData            
        ]);
    }   
}
