<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class ShluckyController
{
    public function number(): Response
    {
        $number = 'atasmutasi';

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }
}